package cat.itb.carlosanchez7e4.uf1.hash;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Exemple {
    public static void main(String[] args) {
        String password = "123456";
        try {
            // a partir de Java 9
            MessageDigest md = MessageDigest.getInstance("SHA3-256");
            byte[] hashInBytes = md.digest(password.getBytes(StandardCharsets.UTF_8));
            /*
            Salting
            SecureRandom random = new SecureRandom();
            byte[] salt = new byte[16];
            random.nextBytes(salt);
            md.update(salt);
             */
            // bytes to hex
            StringBuilder sb = new StringBuilder();
            for (byte b : hashInBytes) {
                sb.append(String.format("%02x", b));
            }
            System.out.println(sb); //sb.toString()
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace(); }
    }
}
