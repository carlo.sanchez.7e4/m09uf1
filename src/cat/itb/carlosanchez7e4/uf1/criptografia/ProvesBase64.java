package cat.itb.carlosanchez7e4.uf1.criptografia;

import java.util.Base64;

public class ProvesBase64 {
    public static void main(String[] args)  {
        String originalInput = "1234";
        String encodedString = Base64.getEncoder().encodeToString(originalInput.getBytes());
        System.out.println("encoded: "+encodedString);
        byte[] decodedBytes = Base64.getDecoder().decode(encodedString);
        String decodedString = new String(decodedBytes);
        System.out.println("decoded: "+decodedString);
    }
}

