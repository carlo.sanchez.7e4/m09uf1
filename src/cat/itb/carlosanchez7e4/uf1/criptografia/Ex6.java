package cat.itb.carlosanchez7e4.uf1.criptografia;

public class Ex6 {
    public static void main(String[] args) throws Exception {
        String msg = "f4fd6ad48f782ecef75f2b50c065278f5fa5abf2ab2f758f16025f141b51545f62ea8be90a093f179f92d69c804ed6fcf21f408dea4380fa3b42e7dd8b169dd81129de98501d361225c41328ffddc0e57756f491dba7223d5f0ad7a5019c8c521b8727ca3fd208f5e73d511752cb04";
        String pwd = "Això és un password del 2022";

        String txt = UtilitatsAES.decryptHex(msg, pwd);
        System.out.println(txt);
    }

}
