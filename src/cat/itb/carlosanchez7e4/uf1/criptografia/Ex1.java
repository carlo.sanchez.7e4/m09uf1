package cat.itb.carlosanchez7e4.uf1.criptografia;

public class Ex1 {
    public static String decrypt(String msg, int x){
        if (x > 26){
            x = x%26;
        }
        else if (x < 0){
            x = (x%26)+26;
        }

        String txt = "";
        int length = msg.length();
        for (int i = 0; i < length; i++){
            char ch = msg.charAt(i);
            if (Character.isLetter(ch)){
                if (Character.isLowerCase(ch)){
                    char c = (char) (ch - x);
                    if (c < 'a'){
                        txt += (char)(ch + (26 - x));
                    }
                    else {
                        txt += c;
                    }
                }
                else if (Character.isUpperCase(ch)){
                    char c = (char)(ch - x);
                    if (c < 'A'){
                        txt += (char)(ch + (26 - x));
                    }
                    else {
                        txt += c;
                    }
                }
            }
            else {
                txt += ch;
            }
        }
        return txt;
    }

    public static String encrypt(String msg, int x){
        if (x > 26){
            x = x%26;
        }
        else if (x < 0){
            x = (x%26)+26;
        }

        String txt = "";
        int length = msg.length();
        for (int i = 0; i < length; i++){
            char ch = msg.charAt(i);
            if (Character.isLetter(ch)){
                if (Character.isLowerCase(ch)){
                    char c = (char) (ch + x);
                    if (c > 'z'){
                        txt += (char)(ch - (26 - x));
                    }
                    else {
                        txt += c;
                    }
                }
                else if (Character.isUpperCase(ch)){
                    char c = (char)(ch + x);
                    if (c > 'Z'){
                        txt += (char)(ch - (26 - x));
                    }
                    else {
                        txt += c;
                    }
                }
            }
            else {
                txt += ch;
            }
        }
        return txt;
    }

    public static void main(String[] args) {
        String msg = "Ls sslunbhanl Qhch ch hwhylpely lu 1996";
        String decipher = decrypt(msg, 7);
        System.out.println(decipher);
    }
}
