package cat.itb.carlosanchez7e4.uf1.criptografia;

public class Ex5 {
    public static void main(String[] args) throws Exception {
        String msg = "RF441svPTZz/YDKHGkVoRBDevEeERIoih6OHosUfcUxrMXCL0JVswGNKTGaOFMSyNeGv/OoEA5yGe6rES8qfwAbQPKrfh6ihM1gRENlzG85EvpMz1ZR9/tguZq195ptmWkA1RM8o";
        String pwd = "Això és un password del 2022";

        String decipher = UtilitatsAES.decrypt(msg, pwd);
        System.out.println(decipher);
    }
}
