package cat.itb.carlosanchez7e4.uf2.killthevirus;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

class PanelPilotes extends JPanel {
    private ArrayList<Virus> pilotes = new ArrayList<Virus>();

    public void addPilota() {
        //es crea una pilota, ara és un thread
        //Virus p = new Virus();
        //se li passa el panell, per saber les dimensions on ha de rebotar
        //p.setElPanelComu(this);
        //s'afegeix a una llista de pilotes, per quan hi hagi vàries
        //pilotes.add(p);
        //es posa en marxa el thread
        //p.start();
    }

    /** Mètode que no es pot canviar de nom. Quan es vol pintar un objecte gràfic sobre un Component
     * de Swing de Java.
     * @param g de tipus Graphics
     */
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D) g;
        /* itera i dibuixa totes les pilotes*/
        for (Virus p : pilotes) {
            //g2.fill(p.dibuixarVirus());
        }
        //Tip per assegurar que el Graphics s'actualitza
        Toolkit.getDefaultToolkit().sync();
    }
}
