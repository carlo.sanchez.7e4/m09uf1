package cat.itb.carlosanchez7e4.uf2.killthevirus;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.Random;



public class Virus implements Runnable{

    private double x;
    private double y;
    private double dx;
    private double dy;
    private final int radi = 30;
    private Random rn  = new Random();
    private boolean virusViu;

    private JPanel elPanelComu;

    /**
     *
     * @param p
     */
    public Virus(JPanel p){
        virusViu = true;
        elPanelComu = p;
        x = rn.nextDouble() * elPanelComu.getBounds().getWidth();
        y = rn.nextDouble() * elPanelComu.getBounds().getHeight();
        dx = (rn.nextDouble()*2)-1;
        dy = (rn.nextDouble()*2)-1;
    }

    /**
     *
     * @param p
     */
    public void setElPanelComu(JPanel p) {
        elPanelComu = p;
    }

    /**
     *
     */
    public void moureVirus() {
        Rectangle2D limits = elPanelComu.getBounds();
        double width = limits.getWidth();
        double height = limits.getHeight();
        //System.out.println(x+"\t"+y);
        double lastx = x;
        double lasty = y;
        x += (virusViu)?dx:0;
        y += (virusViu)?dy:0;
        if (x + radi > width || x + radi < 0) {
            dx = (rn.nextDouble() * 2) - 1;
            dx = (lastx>x && lastx<0 && dx<0)?-dx:dx;
            dx = (lastx<x && lastx>height && dx>0)?-dx:dx;
        }
        if (y + radi > height || y + radi < 0) {
            dy = (rn.nextDouble() * 2) - 1;
            dy = (lasty>y && lasty<0 && dy<0)?-dy:dy;
            dy = (lasty<y && lasty>height && dy>0)?-dy:dy;
        }
    }

    /**
     *
     * @param g
     */
    public void dibuixarVirus(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        //g2.fill(new Ellipse2D.Double(x, y, radi, radi));
        Image virusImage;
        virusImage = (virusViu)?new ImageIcon(getClass().getClassLoader().getResource("virus.png")).getImage():new ImageIcon(getClass().getClassLoader().getResource("virusmort.png")).getImage();
        g2.drawImage(virusImage, (int) x,(int) y, radi, radi, null);
    }

    /**
     * Funció que es cridada quan es vol comprovar si el ratolí a clicat en el virus o no.
     * @param xClick component x de la posició del ratolí
     * @param yClick component y de la posició del ratolí
     */
    public void matar(double xClick, double yClick){
        if((xClick<(x+radi) && yClick>(x-radi)) && (yClick<(y+radi) && yClick>(y-radi))) virusViu=false;
    }

    @Override
    public void run() {
        for (int i = 1; i >0; i++) {
            moureVirus();
            try {
                Thread.sleep(4);
            } catch (Exception e) {
            }
            elPanelComu.repaint();
        }
    }
}
