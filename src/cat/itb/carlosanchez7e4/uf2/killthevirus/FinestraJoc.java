package cat.itb.carlosanchez7e4.uf2.killthevirus;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



public class FinestraJoc extends JFrame {

    private PanelVirus panelVirus = new PanelVirus();

    public FinestraJoc() {
        setBounds(600, 300, 400, 350);
        setTitle("Kill the Virus");
        add(panelVirus , BorderLayout.CENTER);
        JPanel botonera = new JPanel();
        JButton boto1 = new JButton("Pandemic");
        JButton boto2 = new JButton("Exit");
        botonera.add(boto1);
        botonera.add(boto2);
        boto1.addActionListener(new ClickPandemic());
        boto2.addActionListener(new ClickSortir());
        add(botonera, BorderLayout.SOUTH);
    }

    class ClickSortir implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            System.exit(0);
        }
    }

    class ClickPandemic implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            for(int i=0;i<10;i++){
                panelVirus.addVirus();
            }
        }
    }
}