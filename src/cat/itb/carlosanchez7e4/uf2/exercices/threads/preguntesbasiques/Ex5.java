package cat.itb.carlosanchez7e4.uf2.exercices.threads.preguntesbasiques;

public class Ex5 {
    public static void main(String[] args) {
        Runnable r1 = () -> System.out.println("Suma");
        Runnable r2 = () -> System.out.println("Resta");
        Runnable r3 = () -> System.out.println("Multiplica");
        Thread t1 = new Thread(r1);
        t1.start();
        Thread t2 = new Thread(r2);
        t2.start();
        Thread t3 = new Thread(r3);
        t3.start();
    }
}
