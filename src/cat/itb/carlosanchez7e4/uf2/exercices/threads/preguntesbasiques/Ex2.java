package cat.itb.carlosanchez7e4.uf2.exercices.threads.preguntesbasiques;

public class Ex2 {
    public static void main(String[] args) {
        System.out.println(Thread.currentThread().getName());

        MyThread thread = new MyThread();
        thread.run();
    }
}

class MyThread extends Thread{
    @Override
    public void run() {
        System.out.println(getName());
    }
}
