package cat.itb.carlosanchez7e4.uf2.exercices.threads.preguntesbasiques;

public class Ex6 {
    public static void main(String[] args) {
        Cantant c1 =  new Cantant("Julio Iglesias");
        c1.start();

        Thread t1 = new Cantant("Rosalía");
        t1.start();

        (new Cantant("Camilo Sesto")).start();
    }
}

class Cantant extends Thread{
    private String nom;
    public Cantant(String s){
        nom = s;
    }
    @Override
    public void run() {
        cantar();
    }

    public void cantar() {
        System.out.println(nom + " està cantant LA LA LA");
    }
}