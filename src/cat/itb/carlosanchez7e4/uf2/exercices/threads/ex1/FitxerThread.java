package cat.itb.carlosanchez7e4.uf2.exercices.threads.ex1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FitxerThread extends Thread{
    private String name;
    public FitxerThread(String name){
        this.name = name;
    }

    @Override
    public void run(){
        try{
            FileReader fr = new FileReader(this.name);
            BufferedReader bf = new BufferedReader(fr);

            int lines = (int) bf.lines().count();
            bf.close();

            System.out.println("El fitxer " + this.name + " té " + lines + " linees");
        } catch (FileNotFoundException fnfe){
            fnfe.printStackTrace();
        } catch (IOException ioe){
            ioe.printStackTrace();
        }
    }
}
