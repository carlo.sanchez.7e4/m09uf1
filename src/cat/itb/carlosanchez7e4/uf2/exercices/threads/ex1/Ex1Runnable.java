package cat.itb.carlosanchez7e4.uf2.exercices.threads.ex1;

public class Ex1Runnable {
    public static void main(String[] args) {
        Thread t1 = new Thread(new FitxerRunnable("fitxer1.txt"));
        Thread t2 = new Thread(new FitxerRunnable("fitxer2.txt"));
        Thread t3 = new Thread(new FitxerRunnable("fitxer3.txt"));
        t1.start();
        t2.start();
        t3.start();
    }
}
