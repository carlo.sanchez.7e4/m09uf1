package cat.itb.carlosanchez7e4.uf2.exercices.threads.ex1;

public class Prova {
    public static void main(String[] args) {
        FitxerThread ft1 = new FitxerThread("fitxer1.txt");
        FitxerThread ft2 = new FitxerThread("fitxer2.txt");
        FitxerThread ft3 = new FitxerThread("fitxer3.txt");
        ft1.start();
        ft2.start();
        ft3.start();
    }
}
