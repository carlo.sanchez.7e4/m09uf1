package cat.itb.carlosanchez7e4.uf2.exercices.threads.deixardefumar;

public class DeixarDeFumar {
    private static Thread t1, t2;

    public static void main(String[] args) {
        Runnable r1 = DeixarDeFumar::fumador;
        t1 = new Thread(r1);
        t1.start();

        Runnable r2 = DeixarDeFumar::medico;
        t2 = new Thread(r2);
        t2.start();
    }

    private static void medico() {
        try {
            Thread.sleep(3000);
            t1.interrupt();
        } catch (InterruptedException e) {
            System.out.println("Error");
        }
    }

    private static void fumador() {
        int i = 0;
        try {
            while (true) {
                i++;
                System.out.println("Fumo cigarreta... " + i);
                Thread.sleep(1000);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

}