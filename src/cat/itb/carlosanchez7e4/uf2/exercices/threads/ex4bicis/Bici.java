package cat.itb.carlosanchez7e4.uf2.exercices.threads.ex4bicis;

import java.time.Duration;
import java.time.LocalTime;

public class Bici extends Thread{
    //nom del propietari de la bici
    private String nom;
    //moment en que comença el viatge en bici
    private LocalTime inici;
    //distància a recórrer
    private int distancia;
    //temps transcorregut entre inici i completar distància
    private long temps;

    public Bici(String nom, LocalTime inici, int distancia) {
        this.nom = nom;
        this.inici = inici;
        this.distancia = distancia;
    }

    public String getNom() {
        return nom;
    }

    public int getDistancia() {
        return distancia;
    }

    public long getTemps() {
        return temps;
    }

    @Override
    public void run() {
        int counter = 0;
        while (counter < distancia){
            counter++;
        }
        temps = Duration.between(inici, LocalTime.now()).getNano();
    }
}

class Ex4 {
    public static void main(String[] args) throws InterruptedException {
        LocalTime inici = LocalTime.now();
        final int DISTANCIA = 1000;
        Bici b1 = new Bici("Montse", inici, DISTANCIA);
        Bici b2 = new Bici("Fran", inici, DISTANCIA);
        Bici b3 = new Bici("Clara", inici, DISTANCIA);

        b1.start();
        b1.sleep(1000);

        b2.start();
        b2.sleep(1000);

        b3.start();
        b3.sleep(1000);

        printBici(b1);
        printBici(b2);
        printBici(b3);

        Bici[] bicis = new Bici[] { b1, b2, b3 };
        printFastest(bicis);
    }

    private static void printBici(Bici bici){
        System.out.printf("%s ha trigat %d unitats de temps en fer una distància de %d\n", bici.getNom(), bici.getTemps(), bici.getDistancia());
    }

    private static void printFastest(Bici[] bicis) {
        Bici fastest = bicis[0];
        for (Bici b : bicis){
            if (b.getTemps() < fastest.getTemps()){
                fastest = b;
            }
        }

        System.out.printf("La bici més ràpida és la de %s", fastest.getNom());
    }
}