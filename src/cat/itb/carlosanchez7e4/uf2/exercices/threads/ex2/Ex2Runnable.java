package cat.itb.carlosanchez7e4.uf2.exercices.threads.ex2;

import java.util.Scanner;

public class Ex2Runnable {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Primer valor: ");
        long v1 = scanner.nextLong();
        System.out.println("Segon valor: ");
        long v2 = scanner.nextLong();
        System.out.println("Tercer valor: ");
        long v3 = scanner.nextLong();

        Thread t1 = new Thread(new EsPrimerRunnable(v1));
        Thread t2 = new Thread(new EsPrimerRunnable(v2));
        Thread t3 = new Thread(new EsPrimerRunnable(v3));
        t1.start();
        t2.start();
        t3.start();
    }
}
