package cat.itb.carlosanchez7e4.uf2.exercices.threads.ex2;

import java.util.Scanner;

public class Prova {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Primer valor: ");
        long v1 = scanner.nextLong();
        System.out.println("Segon valor: ");
        long v2 = scanner.nextLong();
        System.out.println("Tercer valor: ");
        long v3 = scanner.nextLong();

        EsPrimerThread p1 = new EsPrimerThread(v1);
        EsPrimerThread p2 = new EsPrimerThread(v2);
        EsPrimerThread p3 = new EsPrimerThread(v3);
        p1.start();
        p2.start();
        p3.start();

    }
}
