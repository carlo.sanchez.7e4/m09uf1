package cat.itb.carlosanchez7e4.uf2.repas;

public class Ex1 {
    private final static Object lock = new Object();

    public static void main(String[] args) {
        Thread t1=new Thread(new PrintThread("Una vegada hi havia un gat"));
        Thread t2=new Thread(new PrintThread("Once upon a time in the west"));
        Thread t3=new Thread(new PrintThread("En un lugar de la Mancha"));
        t1.start();
        t2.start();
        t3.start();
    }

    private static class PrintThread implements Runnable{
        private String text;
        private String[] words;

        public PrintThread(String text) {
            this.text = text;
        }

        @Override
        public synchronized void run() {
            synchronized (lock) {
                try {
                    words = text.split(" ");
                    for (String word : words) {
                        System.out.print(word + " ");
                        Thread.sleep(1000);
                    }

                    System.out.println();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}