package cat.itb.carlosanchez7e4.uf2.repas;

public class Ex2 {
    public static void main(String[] args) {
        Nevera nevera=new Nevera(4);

        Thread t1=new Thread(()->nevera.afegeixCervesa(), "Pepi");
        Thread t2=new Thread(()->nevera.beuCervesa(),"Luci");
        Thread t3=new Thread(()->nevera.beuCervesa(),"Bom");
        Thread t4=new Thread(()->nevera.beuCervesa(),"Anna");

        t1.start();
        t2.start();
        t3.start();
        t4.start();

    }

    private static class Nevera {
        private int cerveses;

        public Nevera(int cerveses) {
            this.cerveses = cerveses;
        }

        public synchronized void afegeixCervesa() {
            int num = (int)(Math.random()*6+1);
            System.out.println(Thread.currentThread().getName() + " ha afegit " + num + " cerveses");
            cerveses += num;
        }

        public synchronized void beuCervesa() {
            int num = (int)(Math.random()*6+1);
            if (num > 6) {
                System.out.println(Thread.currentThread().getName() + " no pot beure més de 6 cerveses");
            } else if (num <= cerveses){
                System.out.println(Thread.currentThread().getName() + " ha begut " + num + " cerveses");
                cerveses -= num;
                if (cerveses == 0){
                    System.out.println("S'han acabat les cerveses");
                }
            } else {
                System.out.println("No hi han suficients cerveses");
            }
        }
    }
}
